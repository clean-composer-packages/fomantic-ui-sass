Fomantic UI for Sass
====================

[![Latest Stable Version](https://poser.pugx.org/clean-composer-packages/fomantic-ui-sass/version)](https://packagist.org/packages/clean-composer-packages/fomantic-ui-sass)
[![Total Downloads](https://poser.pugx.org/clean-composer-packages/fomantic-ui-sass/downloads)](https://packagist.org/packages/clean-composer-packages/fomantic-ui-sass)
[![License](https://poser.pugx.org/clean-composer-packages/fomantic-ui-sass/license)](https://packagist.org/packages/clean-composer-packages/fomantic-ui-sass)


This is a automatically generated composer package of [Fomantic UI for Sass](https://github.com/fomantic/Fomantic-UI-SASS).

Installation
------------

```
php composer.phar require clean-composer-packages/fomantic-ui-sass
```

License
-------

**clean-composer-packages/fomantic-ui-sass** is released under the MIT License. See the [LICENSE.md](LICENSE.md) for details.
